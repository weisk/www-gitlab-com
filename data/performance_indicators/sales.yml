- name: Sales Pipeline Coverage
  base_path: "/handbook/sales/performance-indicators/"
  definition: Net ARR of pipeline with close dates in a given period (quarter) divided by Net ARR target.
  target: On day one of the current quarter, total pipeline coverage should be 2.4X, total pipeline for the following quarter should be 2.2X, and total pipeline for 2 
    quarters out should be 1.0X. On day one of the current quarter, stage 3+ pipeline coverage should be 1.5X and stage 3+ pipeline for the following quarter should 
    be 0.8X.
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons: 
    - see latest key meeting deck
- name: Sales Capacity
  base_path: "/handbook/sales/performance-indicators/"
  definition: Deployed street capacity (Sales quota) / company Net ARR Financial Target
  target: greater than 100%
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: ProServe Deal and Dollar Attach Rate
  base_path: "/handbook/sales/performance-indicators/"
  definition: Deal Attach Rate is the number of Professional Services closed won opportunities divided by the number of total closed won opportunities during a given 
    month (by opportunity close date). Dollar Attach Rate is the Professional Services Value of closed won opportunities divided by the Net ARR of closed won 
    opportunities during a given month (by opportunity close date).
  target: 13% for Deal Attach Rate, 6% for Dollar Attach Rate
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - see latest key meeting deck 
- name: Net ARR vs Plan
  base_path: "/handbook/sales/performance-indicators/"
  definition: The year-to-date cumulative sum of Net ARR divided by the year-to-date plan for Net ARR. Details on Net ARR can be found in the 
    sales <a href="https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#net-arr">handbook page</a>.
  target: greater than 1
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: New Logos
  base_path: "/handbook/sales/performance-indicators/"
  definition: Count of new customers by month (excluding SMB)
  target: Monthly targets set by Sales & Marketing
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: Channel Sourced Net ARR
  base_path: "/handbook/sales/performance-indicators/"
  definition: Sum of Net ARR from opportunities that are channel sourced (where sales_qualified_source = 'Channel Generated').
  target: Monthly targets set by Sales & Finance
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: ARPU
  base_path: "/handbook/sales/performance-indicators/"
  definition: The number of contracted users on active paid subscriptions. Excludes OSS, Education, Core and other non-paid users. The data source is Zuora.
  target: 2% increase per month
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: Percent of Ramped Reps at or Above Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramped if their tenure in a particular role is more than the duration of their assigned quota ramp schedule. The percentage of 
    attainment calculation is the net ARR from closed won opportunities in a particular month divided by the quota. Industry average is 45%-65% of reps achieving 100% 
    of quota. Details on quota ramp schedule can be found in the 
    sales <a href="https://about.gitlab.com/handbook/sales/commissions/#month-1-ramp-schedule"> handbook page</a>.
  target: greater than 55%
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - see latest key meeting deck
- name: Percent of Ramping Reps at or Above 70% of Quota
  base_path: "/handbook/sales/performance-indicators/"
  definition: A Rep is considered ramping if their tenure in a particular role is less than the duration of their assigned quota ramp schedule. The percentage of    
    attainment calculation is the net ARR from closed won opportunities in a particular month divided by the quota. Details on quota ramp schedule can be found in the 
    sales <a href="https://about.gitlab.com/handbook/sales/commissions/#month-1-ramp-schedule">handbook page</a>.
  target: greater than 70%
  org: Sales
  is_key: true
  public: true
  health:
    level: 0
    reasons:
    - see latest key meeting deck
- name: Churn and Contraction / ATR
  base_path: "/handbook/sales/performance-indicators/"
  definition: Sum of Net ARR of Churn and Contraction Opportunities (where order type = Contraction, Churn Partial, or Churn Full) divided by the amount of Net ARR 
    that was available to renew in a specified time period (sum of ARR Basis of opportunities with renewal dates in specified time period)
  target: Quarterly target set by Sales & Finance
  org: Sales
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: Net Retention
  base_path: "/handbook/sales/performance-indicators/"
  definition: Net Retention Calculation is extensively explained in the 
    <a href="https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn">Customer Success's Vision page</a>.
  target: Above 145%
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - see latest key meeting deck
- name: CAC Ratio
  base_path: "/handbook/sales/performance-indicators/"
  definition: Sales and Marketing expenses (including the cost of free users of gitlab.com) over trailing twelve months divided by current quarter ARR annualized 
    minus ARR from the same period in the prior year annualized. Details on CAC can be found in the 
    sales <a href="https://about.gitlab.com/handbook/sales/sales-term-glossary/#customer-acquisition-cost-cac">handbook page</a>.
  target: Monthly target set by Finance
  org: Sales
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - see latest key meeting deck
