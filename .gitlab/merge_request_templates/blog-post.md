<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. -->

<!-- All blog posts should have a corresponding issue. Create an issue now if you haven't already, and add the link in place of the placeholder link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/XXXX

### Checklist for writer

- [ ] Link to issue added, and set to close when this MR is merged
- [ ] Due date and marketing milestone (e.g. `Mktg: 2021-03-28`) added for the desired publish date
- [ ] Please suggest a [target keyword](https://about.gitlab.com/handbook/marketing/inbound-marketing/content/editorial-team/#seo-guidelines) for your post for SEO: ___
- [ ] Please add links to three related blog posts, GitLab issues, documentation or other related content so the reader can learn more at the bottom of the post. (We will take care of the formatting.)
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP
- [ ] [Blog post file formatted correctly](https://about.gitlab.com/handbook/marketing/blog/#formatting-guidelines), including any accompanying images
- [ ] All relevant [frontmatter](https://about.gitlab.com/handbook/marketing/blog/#frontmatter) included
- [ ] Review app checked for any formatting issues
- [ ] Reviewed by fellow team member
- [ ] If approval before publishing is required
  - [ ]  Any required internal or external approval for this blog post has been granted (please leave a comment with details)  
- [ ] Assign to the Editorial team member who reviewed your pitch issue for final review (If they are on PTO and your post is time sensitive, please share your MR in #content on Slack to ask for another reviewer.)

After the blog has been published:
- [ ] Share on your social media channels
   - Add `?utm_medium=social&utm_campaign=blog&utm_content=advocacy` to the end of the blog URL when you share on social media, for data tracking. Your link should look like this:https://about.gitlab.com/blog/20xx/xx/xx/blog-title/?utm_medium=social&utm_campaign=blog&utm_content=advocacy
- [ ] After you've shared on your social media profiles, select one of the posts and link it in the #social_media_posts Slack channel for everyone to engage with your post. The GitLab social team may engage or even share your social media post to amplify the work.
- To learn more about how to use your own social media channels for GitLab, [check out our team member social media guidelines here](https://about.gitlab.com/handbook/marketing/social-media-guidelines/).

/label ~"blog post"
