---
layout: handbook-page-toc
title: "People Analytics Overview"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

---
## <i class="far fa-users" id="biz-tech-icons"></i> People Analytics


**The study of people at work! Human resource departments are flipping their approach to organizational behavior and instead of using judgements or opinions to make decisions are making it fact based with the power of data!**

**The benefits of data in the people space:**

<html>   
<head>
<style>
.flex-container {
  display: flex;
  background-color: #554488;
  text-align: center;
}

.flex-container > div {
  background-color: white;
  color: gray;
  margin: 10px;
  padding: 5px;
  font-size: 14px;
}
</style>
</head>
<body>

<div class="flex-container">
  <div>Build a more streamlined recruiting process that helps GitLab build a strong and diverse team, as well as provides candidates going through the process a well-crafted experience.</div>
  <div>Drive teams to constantly be improving the experience of GitLab team members, and we mean it when we say "Everyone can contribute." We want individuals to thrive in their careers here and using engagement surveys and KPIs to drive our team goal we put people at the forefront. </div>
  <div>Tie in the other data! Whether it be sales data, engineering data, etc it is all important to us! We want to understand how we can help team members grow in their journey and help leaders understand the impact of their organizations.</div>  
</div>

<h2><strong>People Data Sources:</strong></h2>
<p>BambooHR</p>
<p>-&nbsp;HR management system.&nbsp;</p>
<p>Greenhouse</p>
<p>- Recruiting and Applicant Management System</p>
<p>PTO By Roots</p>
<p>- A slack application that captures team member time off</p>
