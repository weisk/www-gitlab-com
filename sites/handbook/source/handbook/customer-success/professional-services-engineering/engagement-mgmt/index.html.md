---
layout: handbook-page-toc
title: "Professional Services Engagement Management"
description: "Describes the workflow and responsibilities of the GitLab Professional Services Engagement Manager."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Professional Services (PS) Engagement Manager is responsible for helping Solution Architects (SAs) get custom Statement of Works (SoWs) approved. The process typically includes the following activities.

* An initial meeting with the SA to understand the customer’s expectation
* Updating the Services Calculator generated SoW based on the initial SA meeting
* Updating the draft SoW based on feedback from the SA
* Creating a cost estimate
* Providing clarification to the PS Director in order to obtain approval for the SoW 
